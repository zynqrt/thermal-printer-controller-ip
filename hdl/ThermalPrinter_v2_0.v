
`timescale 1 ns / 1 ps

	module ThermalPrinter_v2_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI_CTRL
		parameter integer C_S00_AXI_CTRL_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_CTRL_ADDR_WIDTH	= 6,

		// Parameters of Axi Slave Bus Interface S01_AXI_DATA
		parameter integer C_S01_AXI_DATA_ID_WIDTH	= 1,
		parameter integer C_S01_AXI_DATA_DATA_WIDTH	= 32,
		parameter integer C_S01_AXI_DATA_ADDR_WIDTH	= 6,
		parameter integer C_S01_AXI_DATA_AWUSER_WIDTH	= 0,
		parameter integer C_S01_AXI_DATA_ARUSER_WIDTH	= 0,
		parameter integer C_S01_AXI_DATA_WUSER_WIDTH	= 0,
		parameter integer C_S01_AXI_DATA_RUSER_WIDTH	= 0,
		parameter integer C_S01_AXI_DATA_BUSER_WIDTH	= 0
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI_CTRL
		input wire  s00_axi_ctrl_aclk,
		input wire  s00_axi_ctrl_aresetn,
		input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_awaddr,
		input wire [2 : 0] s00_axi_ctrl_awprot,
		input wire  s00_axi_ctrl_awvalid,
		output wire  s00_axi_ctrl_awready,
		input wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_wdata,
		input wire [(C_S00_AXI_CTRL_DATA_WIDTH/8)-1 : 0] s00_axi_ctrl_wstrb,
		input wire  s00_axi_ctrl_wvalid,
		output wire  s00_axi_ctrl_wready,
		output wire [1 : 0] s00_axi_ctrl_bresp,
		output wire  s00_axi_ctrl_bvalid,
		input wire  s00_axi_ctrl_bready,
		input wire [C_S00_AXI_CTRL_ADDR_WIDTH-1 : 0] s00_axi_ctrl_araddr,
		input wire [2 : 0] s00_axi_ctrl_arprot,
		input wire  s00_axi_ctrl_arvalid,
		output wire  s00_axi_ctrl_arready,
		output wire [C_S00_AXI_CTRL_DATA_WIDTH-1 : 0] s00_axi_ctrl_rdata,
		output wire [1 : 0] s00_axi_ctrl_rresp,
		output wire  s00_axi_ctrl_rvalid,
		input wire  s00_axi_ctrl_rready,

		// Ports of Axi Slave Bus Interface S01_AXI_DATA
		input wire  s01_axi_data_aclk,
		input wire  s01_axi_data_aresetn,
		input wire [C_S01_AXI_DATA_ID_WIDTH-1 : 0] s01_axi_data_awid,
		input wire [C_S01_AXI_DATA_ADDR_WIDTH-1 : 0] s01_axi_data_awaddr,
		input wire [7 : 0] s01_axi_data_awlen,
		input wire [2 : 0] s01_axi_data_awsize,
		input wire [1 : 0] s01_axi_data_awburst,
		input wire  s01_axi_data_awlock,
		input wire [3 : 0] s01_axi_data_awcache,
		input wire [2 : 0] s01_axi_data_awprot,
		input wire [3 : 0] s01_axi_data_awqos,
		input wire [3 : 0] s01_axi_data_awregion,
		input wire [C_S01_AXI_DATA_AWUSER_WIDTH-1 : 0] s01_axi_data_awuser,
		input wire  s01_axi_data_awvalid,
		output wire  s01_axi_data_awready,
		input wire [C_S01_AXI_DATA_DATA_WIDTH-1 : 0] s01_axi_data_wdata,
		input wire [(C_S01_AXI_DATA_DATA_WIDTH/8)-1 : 0] s01_axi_data_wstrb,
		input wire  s01_axi_data_wlast,
		input wire [C_S01_AXI_DATA_WUSER_WIDTH-1 : 0] s01_axi_data_wuser,
		input wire  s01_axi_data_wvalid,
		output wire  s01_axi_data_wready,
		output wire [C_S01_AXI_DATA_ID_WIDTH-1 : 0] s01_axi_data_bid,
		output wire [1 : 0] s01_axi_data_bresp,
		output wire [C_S01_AXI_DATA_BUSER_WIDTH-1 : 0] s01_axi_data_buser,
		output wire  s01_axi_data_bvalid,
		input wire  s01_axi_data_bready,
		input wire [C_S01_AXI_DATA_ID_WIDTH-1 : 0] s01_axi_data_arid,
		input wire [C_S01_AXI_DATA_ADDR_WIDTH-1 : 0] s01_axi_data_araddr,
		input wire [7 : 0] s01_axi_data_arlen,
		input wire [2 : 0] s01_axi_data_arsize,
		input wire [1 : 0] s01_axi_data_arburst,
		input wire  s01_axi_data_arlock,
		input wire [3 : 0] s01_axi_data_arcache,
		input wire [2 : 0] s01_axi_data_arprot,
		input wire [3 : 0] s01_axi_data_arqos,
		input wire [3 : 0] s01_axi_data_arregion,
		input wire [C_S01_AXI_DATA_ARUSER_WIDTH-1 : 0] s01_axi_data_aruser,
		input wire  s01_axi_data_arvalid,
		output wire  s01_axi_data_arready,
		output wire [C_S01_AXI_DATA_ID_WIDTH-1 : 0] s01_axi_data_rid,
		output wire [C_S01_AXI_DATA_DATA_WIDTH-1 : 0] s01_axi_data_rdata,
		output wire [1 : 0] s01_axi_data_rresp,
		output wire  s01_axi_data_rlast,
		output wire [C_S01_AXI_DATA_RUSER_WIDTH-1 : 0] s01_axi_data_ruser,
		output wire  s01_axi_data_rvalid,
		input wire  s01_axi_data_rready
	);
// Instantiation of Axi Bus Interface S00_AXI_CTRL
	ThermalPrinter_v2_0_S00_AXI_CTRL # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_CTRL_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_CTRL_ADDR_WIDTH)
	) ThermalPrinter_v2_0_S00_AXI_CTRL_inst (
		.S_AXI_ACLK(s00_axi_ctrl_aclk),
		.S_AXI_ARESETN(s00_axi_ctrl_aresetn),
		.S_AXI_AWADDR(s00_axi_ctrl_awaddr),
		.S_AXI_AWPROT(s00_axi_ctrl_awprot),
		.S_AXI_AWVALID(s00_axi_ctrl_awvalid),
		.S_AXI_AWREADY(s00_axi_ctrl_awready),
		.S_AXI_WDATA(s00_axi_ctrl_wdata),
		.S_AXI_WSTRB(s00_axi_ctrl_wstrb),
		.S_AXI_WVALID(s00_axi_ctrl_wvalid),
		.S_AXI_WREADY(s00_axi_ctrl_wready),
		.S_AXI_BRESP(s00_axi_ctrl_bresp),
		.S_AXI_BVALID(s00_axi_ctrl_bvalid),
		.S_AXI_BREADY(s00_axi_ctrl_bready),
		.S_AXI_ARADDR(s00_axi_ctrl_araddr),
		.S_AXI_ARPROT(s00_axi_ctrl_arprot),
		.S_AXI_ARVALID(s00_axi_ctrl_arvalid),
		.S_AXI_ARREADY(s00_axi_ctrl_arready),
		.S_AXI_RDATA(s00_axi_ctrl_rdata),
		.S_AXI_RRESP(s00_axi_ctrl_rresp),
		.S_AXI_RVALID(s00_axi_ctrl_rvalid),
		.S_AXI_RREADY(s00_axi_ctrl_rready)
	);

// Instantiation of Axi Bus Interface S01_AXI_DATA
	ThermalPrinter_v2_0_S01_AXI_DATA # ( 
		.C_S_AXI_ID_WIDTH(C_S01_AXI_DATA_ID_WIDTH),
		.C_S_AXI_DATA_WIDTH(C_S01_AXI_DATA_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S01_AXI_DATA_ADDR_WIDTH),
		.C_S_AXI_AWUSER_WIDTH(C_S01_AXI_DATA_AWUSER_WIDTH),
		.C_S_AXI_ARUSER_WIDTH(C_S01_AXI_DATA_ARUSER_WIDTH),
		.C_S_AXI_WUSER_WIDTH(C_S01_AXI_DATA_WUSER_WIDTH),
		.C_S_AXI_RUSER_WIDTH(C_S01_AXI_DATA_RUSER_WIDTH),
		.C_S_AXI_BUSER_WIDTH(C_S01_AXI_DATA_BUSER_WIDTH)
	) ThermalPrinter_v2_0_S01_AXI_DATA_inst (
		.S_AXI_ACLK(s01_axi_data_aclk),
		.S_AXI_ARESETN(s01_axi_data_aresetn),
		.S_AXI_AWID(s01_axi_data_awid),
		.S_AXI_AWADDR(s01_axi_data_awaddr),
		.S_AXI_AWLEN(s01_axi_data_awlen),
		.S_AXI_AWSIZE(s01_axi_data_awsize),
		.S_AXI_AWBURST(s01_axi_data_awburst),
		.S_AXI_AWLOCK(s01_axi_data_awlock),
		.S_AXI_AWCACHE(s01_axi_data_awcache),
		.S_AXI_AWPROT(s01_axi_data_awprot),
		.S_AXI_AWQOS(s01_axi_data_awqos),
		.S_AXI_AWREGION(s01_axi_data_awregion),
		.S_AXI_AWUSER(s01_axi_data_awuser),
		.S_AXI_AWVALID(s01_axi_data_awvalid),
		.S_AXI_AWREADY(s01_axi_data_awready),
		.S_AXI_WDATA(s01_axi_data_wdata),
		.S_AXI_WSTRB(s01_axi_data_wstrb),
		.S_AXI_WLAST(s01_axi_data_wlast),
		.S_AXI_WUSER(s01_axi_data_wuser),
		.S_AXI_WVALID(s01_axi_data_wvalid),
		.S_AXI_WREADY(s01_axi_data_wready),
		.S_AXI_BID(s01_axi_data_bid),
		.S_AXI_BRESP(s01_axi_data_bresp),
		.S_AXI_BUSER(s01_axi_data_buser),
		.S_AXI_BVALID(s01_axi_data_bvalid),
		.S_AXI_BREADY(s01_axi_data_bready),
		.S_AXI_ARID(s01_axi_data_arid),
		.S_AXI_ARADDR(s01_axi_data_araddr),
		.S_AXI_ARLEN(s01_axi_data_arlen),
		.S_AXI_ARSIZE(s01_axi_data_arsize),
		.S_AXI_ARBURST(s01_axi_data_arburst),
		.S_AXI_ARLOCK(s01_axi_data_arlock),
		.S_AXI_ARCACHE(s01_axi_data_arcache),
		.S_AXI_ARPROT(s01_axi_data_arprot),
		.S_AXI_ARQOS(s01_axi_data_arqos),
		.S_AXI_ARREGION(s01_axi_data_arregion),
		.S_AXI_ARUSER(s01_axi_data_aruser),
		.S_AXI_ARVALID(s01_axi_data_arvalid),
		.S_AXI_ARREADY(s01_axi_data_arready),
		.S_AXI_RID(s01_axi_data_rid),
		.S_AXI_RDATA(s01_axi_data_rdata),
		.S_AXI_RRESP(s01_axi_data_rresp),
		.S_AXI_RLAST(s01_axi_data_rlast),
		.S_AXI_RUSER(s01_axi_data_ruser),
		.S_AXI_RVALID(s01_axi_data_rvalid),
		.S_AXI_RREADY(s01_axi_data_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
