

proc generate {drv_handle} {
	xdefine_include_file $drv_handle "xparameters.h" "ThermalPrinter" "NUM_INSTANCES" "DEVICE_ID"  "C_S00_AXI_CTRL_BASEADDR" "C_S00_AXI_CTRL_HIGHADDR" "C_S01_AXI_DATA_BASEADDR" "C_S01_AXI_DATA_HIGHADDR"
}
