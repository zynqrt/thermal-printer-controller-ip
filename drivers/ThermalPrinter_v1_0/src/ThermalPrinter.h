
#ifndef THERMALPRINTER_H
#define THERMALPRINTER_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"

#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG0_OFFSET 0
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG1_OFFSET 4
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG2_OFFSET 8
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG3_OFFSET 12
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG4_OFFSET 16
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG5_OFFSET 20
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG6_OFFSET 24
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG7_OFFSET 28
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG8_OFFSET 32
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG9_OFFSET 36
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG10_OFFSET 40
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG11_OFFSET 44
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG12_OFFSET 48
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG13_OFFSET 52
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG14_OFFSET 56
#define THERMALPRINTER_S00_AXI_CTRL_SLV_REG15_OFFSET 60


/**************************** Type Definitions *****************************/
/**
 *
 * Write/Read 32 bit value to/from THERMALPRINTER user logic memory (BRAM).
 *
 * @param   Address is the memory address of the THERMALPRINTER device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 * 	void THERMALPRINTER_mWriteMemory(u32 Address, u32 Data)
 * 	u32 THERMALPRINTER_mReadMemory(u32 Address)
 *
 */
#define THERMALPRINTER_mWriteMemory(Address, Data) \
    Xil_Out32(Address, (u32)(Data))
#define THERMALPRINTER_mReadMemory(Address) \
    Xil_In32(Address)

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the THERMALPRINTERinstance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus THERMALPRINTER_Mem_SelfTest(void * baseaddr_p);

/**
 *
 * Write a value to a THERMALPRINTER register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the THERMALPRINTERdevice.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void THERMALPRINTER_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define THERMALPRINTER_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a THERMALPRINTER register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the THERMALPRINTER device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 THERMALPRINTER_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define THERMALPRINTER_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the THERMALPRINTER instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus THERMALPRINTER_Reg_SelfTest(void * baseaddr_p);

#endif // THERMALPRINTER_H
